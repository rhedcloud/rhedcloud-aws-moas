# (RHEDcloud) Emory AWS Message Objects and Services

This repository contains the artifacts needed for the pipeline to generate Java MOA classes for use in OpenEAI Applications and Services that integrate with AWS.

## Pipeline output

Generate and build all moas

This step creates the packages containing MOA classes for the AWS integration. It is stored in the [Downloads](https://bitbucket.org/rhedcloud/rhedcloud-aws-moas/downloads/) section of the repository. Several packages are created but the one that apps and services will need is called ```rhedcloud-aws-moa-master-{Build Number}.jar```
	
The Enterprise Objects (EO) are generated then copied to the resources repo, [rhedcloud-aws-moas-resource](https://bitbucket.org/rhedcloud/rhedcloud-aws-moas-resource/src/master/). Then, the customized EOs from the ```config``` folder are copied and overlaid onto those that were generated.

